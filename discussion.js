//Aggregation Pipelines
    //Aggregation is typically done in 2-3 steps. Each step is a called a pipeline.
    //The first pipeline is $match. $match is used  to pass documents or get documents which satisfies or matches our condition.
    //syntax: {$match: {field:<value>}}
    //$group - allows us to group together documents and create an analysis out of these grouped documents.
    //_id: in group stage, we are essentially associating an id to our results.

    //_id: $supplier - we grouped our matched documents based on the value of their supplier field.
    //_id: $<field> - groups documents based on the value of the indicated field.

    //we created a new field for this aggregated result called totalStocks.

    //We used $sum to get the sum of all the values of the grouped documents per the indicated field: $stocks

db.fruits.aggregate([
    {$match: {onSale:true}},//apple,mango,kiwi,dragon fruit
    
    //supplier: Red Farms - apple, dragon fruit - totalStocks: stocks:20+stocks:10
    //supplier: Green Farming - kiwi - totalStocks: stocks:25
    //supplier: Yellow Farms - mango - totalStocks: stocks: 10
    {$group: {_id:"$supplier",totalStocks:{$sum: "$stocks"}}}
])    

db.fruits.aggregate([
    {$match: {onSale:true}},
    //banana,mango
    
    //when you add a definite value of our own for the _id in group stage, 

    //we will have only 1 group from our matched documents. 
    //totalOnSaleStocks, totalStocks:{stocks: stocks:20+stocks:25+stocks:10+stocks:10+stocks:15}
    //{$group: {_id:"totalOnSaleStocks",totalStocks:{$sum: "$stocks"}}}

    //Same goes if the _id is null.
    //{$group: {_id:null,totalStocks:{$sum: "$stocks"}}}

    //What if the _id provided, is a value common to some of our documents?
    //It will be considered as a definite value and just group our documents in a single group.

    {$group: {_id:"Yellow Farms",totalStocks:{$sum: "$stocks"}}}

])

//$avg - $avg is an operator mostly used for the $group stage.
//$avg gets the avg of the numerical values of the indicated field
//in grouped documents.
db.fruits.aggregate([

    {$match:{onSale:true}},
    //supplier:Red Farms Inc - apple - avgStock: stocks:20/<numberOfDocuments>
    //supplier: Yellow Farms - banana,mango - avgStock stocks:15+stocks:10/2
    {$group:{_id:"$supplier", avgStock:{$avg: "$stocks"}}}

])

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",avgPrice:{$avg:"$price"}}}

])

//$max - will allow us to get the highest value out of 
//all the values in a given field
//in grouped documents.

//highest number of stock for all items on sale:
db.fruits.aggregate([
    
    {$match: {onSale:true}},
    {$group: {_id:"highestStockOnSale", maxStock:{$max: "$stocks"}}}

])

    